/* ========================================================================== */
/*                                                                            */
/*   Filename.c                                                               */
/*   (c) 2012 Author                                                          */
/*                                                                            */
/*   Description                                                              */
/*                                                                            */
/* ========================================================================== */


#include <cstdlib>
#include <iostream>

using namespace std

struct Pole {
	float * data;
	int length;
}

Pole * create(int length) {                    // vytvor� pole, parametr pocet predmetu (pocet c�sel v poli)
	Pole * result = new Pole;                    //result je adresa ulo�en� nov�ho pole
	result->data = new float[length];            
	result->length = length;
	return result;
}

float get(Pole * pole, int index) {
	if (index < 0 || index >= pole->length) {
		std::cout << "Index mimo rozsah" << std::endl;
		exit(EXIT_FAILURE);
	}
	return pole->data[index];
}

void set(Pole * pole, int index, float value) {        //  nahrazen� prvku
	if (index < 0) {
		cout << "Index mimo rozsah" << std::endl;
		exit(EXIT_FAILURE);
	}
	if (index >= pole->length) {                           //do dat v poli narveme data s upravenym prvkem
		float * newData = new float[index + 1];
		for (int i = 0; i < pole->length; ++i)
			newData[i] = pole->data[i];
		delete[] pole->data;                                 // smazani starejch dat na zasobniku
		pole->data = newData;
		pole->length = index + 1;
	}
	pole->data[index] = value;                             // o index vy��� m�sto v adrese kam strc�me na�i hodnotu
}

void insert(Pole* pole, int index, float value)          //  vlo�en� nov�ch prvku  ddo pole
{
	int t = 0;
	if (index < 0 || index > pole->length) {
		std::cout << "Index mimo rozsah" << std::endl;
		exit(EXIT_FAILURE);
	}
	float * newData = new float[pole->length + 1];
	for (int i = 0; i < pole->length; ++i)
	{
		if (i == index)
		{
			t = 1;
		}
		newData[i + t] = pole->data[i];  // i+t vytvori prazdny misto na indexu insertu a posune vsechny dalsi prvky o 1 nahoru
	}
	newData[index] = value;
	delete[] pole->data;
	pole->data = newData;
	++pole->length;
}

void pop(Pole* pole, int index)                   // jako se, ale zru�� nejak� prvek A v�echny nad n�m posune o index n�
{
	int i = index;
	while (i < pole->length)
	{
		pole->data[i] = pole->data[i + 1];
		++i;
	}
	--pole->length;
}
                                                  //       print
void show(Pole* p)
{
	int i = 0;
	cout << "[";
	while (i + 1 < p->length)
	{
		std::cout << get(p, i) << ", ";
		++i;
	}
	cout << get(p, i) << "]" << std::endl;
}


void swap(Pole* p, int left, int right){               // vymenen� dvou prvku v poli
	float tmp = get(p, right);
	set(p, right, get(p, left));
	set(p, left, tmp);
}

int main(int argc, char** argv) {                    //
	char intake;                                       //input na pr�k. r�dce
	int capacity;                                      //kapacita batohu
	Pole * p = create(0);                              //na adrese p nov� pole (nulov� d�lka zat�m))
	string input;
	cout << "Vyber si kde chce� zad�vat data: (v kodu ('k') nebo radce ('r')) " << endl;
  cin >> intake;

	if (intake == 'k')                                 //pole zadan� v k�du
	{
		capacity = 30;
		set(p, 0, 1);
		set(p, 1, 8);
		set(p, 2, 40);
		set(p, 3, 100);
		set(p, 4, 45);
		set(p, 5, 87);
		set(p, 6, 81);
		set(p, 7, 5);
		set(p, 8, 9);
		set(p, 9, 64);
		set(p, 10, 3);
		set(p, 11, 50);
		set(p, 12, 7);
		set(p, 13, 8);
		set(p, 14, 16);
		set(p, 15, 18);
		set(p, 16, 30);
		set(p, 17, 22);
		set(p, 18, 24);
		set(p, 19, 24);
	}
	else
	{
		cout << "Kapacita: ";
		cin >> capacity;                             // kapacita bathu
		int i = 0;
		int object;                                // aktu�ln� prvek 
		while (true)
		{
			cout << "Hmotnost predmetu " << i << ": ";
			cin >> object;
			if (object == 0)
			{                
        cout << "Zadali jste nula" << edl;
				break;       
			}
			else
			{
				set(p, i * 2, capacity);
			}
			cout << "Hmotnost predmetu " << i << ": ";
			cin >> capacity;
			set(p, i * 2 + 1, object);
			++i;
		}
	}
	int ITEMAMNT = p->length / 2;                         // pomocna promenn� pro d�lku pole /2 (v poli jsou na ka�d� predmet 2 indexy)
  long int cases = pow(2, ITEMAMNT);                    // pocet pr�padu je 2**pocet predmetu
	long int case_no = 0;                                 // pr�ve prohl�en� pr�pad
	unsigned short int best_case = 0;                     // pr�pad, kde byl (zat�m) nejlep�� v�sledek)
	unsigned short int best_value = 0;                    // na konci nejlep�� hodota, kter� se vyp�e
	unsigned short int counter = ITEMAMNT - 1;            // pocitadlo
	int x = capacity;
	unsigned long int pom = caseno;                       // 
	int value = 0;                                        // aktu�ln� hodnota batohu
	while (case_no <= cases)                              // jakmile projedeme v�echnykombinace
	{
		counter = ITEMAMNT - 1;                             //
		x = capacity;
		pom = case_no;
		value = 0;
		while (i >= 0 && pom >= 0)                          // Prevzato od Adama, muj nefungoval a neyl jsem schopen prij�t na jin� algoritus ->
		{
			if (pom >= pow(2, i))  // tohle zjisti jestli 2^i je nejvetsi 2^n mensi nez pom, cili ze je na prislusnym miste v binarce
			{
				if (x >= get(p, i * 2))  // resp. jestli se item jeste vejde
				{
					x -= get(p, i * 2);
					value += get(p, (i * 2) + 1);
					pom -= pow(2, i);

				}
				else  // kdyz se kdykoli neco nevejde, muzeme ten pripad rovnou zahodit
				{
					value = 0;
					break;
				}
			}
			--counter;
		}
		if (value > bestvalue)  // proste nahradi kdyz novy reseni je lepsi
		{
			best_case = case_no;
			best_value = value;
		}
		++caseno;
	}
	counter = ITEMAMNT - 1;
	pom = bestcase;
	int totalw = 0;
	show(p);
	while (counter >= 0)  // tohle je jenom na printovani, zas to zjisti jak vypada cislo v binarce a podle toho vybere itemy, ktery v tom pripade jsou v batohu
	{
		if (pom >= pow(2, i))
		{
			cout << get(p, i * 2) << " : " << get(p, i * 2 + 1) << std::endl;
			pom -= pow(2, i);
			totalw += get(p, i * 2);  // tohle jenom pocita celkovou hmotnost pro prehlednost vypisu
		}
		--i;
	}
	cout << "---------" << std::endl << totalw << "/" << CAP << " : " << bestvalue;
		
	cin >> x;
	return EXIT_SUCCESS;

		